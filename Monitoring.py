import psutil
import argparse
import parser
import args
import Alarm
print("\n Willkommen im Monitoringtool! \n")


def HDD():
    a = round (psutil.disk_usage ("C:\ ").free / (1024 ** 3), 2)
    return a

def RAM():
    b = round (psutil.virtual_memory ().available / (1024 ** 3), 2)
    return b

def CPU():
    c = psutil.cpu_percent (1)
    return c


def parser():
    parser = argparse.ArgumentParser (prog='__main__', description="Statusabfrage")

    parser.add_argument ('-hdd', action="store_true", help="Information about the status.")
    parser.add_argument ('-ram', action="store_true", help="Information about the status.")
    parser.add_argument ('-cpu', action="store_true", help="Information about the status.")
    args = parser.parse_args ()
    return args

# das Modul kann als eigenständiges Programm vom Benutzer ausgeführt werden
if __name__ == "__main__":
    args = parser()

    if args.ram:
        print ("Available RAM =", RAM (), "GB")

    elif args.hdd:
        print ("Available storage =", HDD (), "GB")

    elif args.cpu:
        print ("Current CPU =", CPU (), "%")

    else:
        print ("Available RAM =", RAM (), "GB")
        print ("Available storage =", HDD (), "GB")
        print ("Current CPU =", CPU (), "%")

    Alarm.logfile ()
