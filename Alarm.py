import Monitoring
import logging
import configparser
import psutil
import shutil
import time

def logfile():
    config = configparser.ConfigParser()
    config.read("config.ini")
    RAMsoftlimit = float(config.get("Limit", "RAMsoftlimit"))
    RAMhardlimit = float(config.get("Limit", "RAMhardlimit"))
    HDDsoftlimit = float(config.get("Limit", "HDDsoftlimit"))
    HDDhardlimit = float(config.get("Limit", "HDDhardlimit"))
    CPUsoftlimit = float(config.get("Limit", "CPUsoftlimit"))
    CPUhardlimit = float(config.get("Limit", "CPUhardlimit"))

    logging.basicConfig(filename='Logdatei.log', level=logging.DEBUG, format='%(asctime)s %(message)s')
    logging.info ('Logging app started')
    logging.warning ('Warning')

    CPU = Monitoring.CPU()
    RAM = Monitoring.RAM()
    HDD = Monitoring.HDD()

    if CPUsoftlimit < CPU < CPUhardlimit:
        print("High CPU usage at", CPU, "%")
        logging.warning("High CPU usage at %s.", CPU)

    elif CPUsoftlimit < CPU > CPUhardlimit:
        print("Warning! Very high CPU usage at", CPU, "%")
        logging.warning("Warning! Very high CPU usageat %s.", CPU)

    if RAMsoftlimit > RAM > RAMhardlimit:
        print("Low available RAM at", RAM, "GB")
        logging.warning("Low available RAM at %s GB", RAM)

    elif RAMsoftlimit > RAM < RAMhardlimit:
        print("Warning! Very low available RAM at", RAM, "GB")
        logging.warning("Warning! Very low available RAM at %s GB", RAM)

    if HDDsoftlimit > HDD > HDDhardlimit:
        print("Low available storage capacity at", HDD, "GB")
        logging.warning("Low available storage capacity at %s GB", HDD)

    elif HDDsoftlimit > HDD < HDDhardlimit:
        print("Warning! Very low available storage capacity at", HDD, "GB")
        logging.warning("Warning! Very low available storage capacity at %s GB", HDD)


